<?php

namespace Hn\HiDPI;


use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\ProcessedFile;
use TYPO3\CMS\Core\Resource\Processing\TaskInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Service\ImageService;
use TYPO3\CMS\Extbase\SignalSlot\Dispatcher;
use TYPO3Fluid\Fluid\Core\ViewHelper\TagBuilder;

/**
 * Class HiDpiHandler
 *
 * It runs on a few assumptions:
 * - HiDPI is mostly used on (and must be optimized for) mobile devices so images over 1440px wide are wasted
 * - images aren't upscaled, so if an image is defined as 300px wide,
 *   it will always be at most 300(none physical)px wide
 * - all templates are developed with 1x scaling factor for the dektop
 * - perfect pixel matching on HiDPI screens is not necessary
 *
 * Trough those assumptions I can use the old srcset="2x" syntax
 * to get fairly good results without any integration work.
 *
 * Because I assume that my screen is probably never bigger than 1440 pixels, I can limit the resolution
 * of the HiDPI Image. This solves the issue of having a big banner image having a HiDPI variant
 * that is huge loading on mobile. The sizes variant could prevent this but that would require more integration work.
 * The reason i don't use 1440 pixels is because of assumption 3 further down.
 * This assumption breaks on a desktop and only small images will be displayed at higher DPI.
 * But in those cases I'd argue that you don't need a high resolution banner across your page.
 *
 * Because I assume that my images are never upscaled from the dimensions given here,
 * I can compress the HiDPI images stronger than the normal image resulting in the page not getting
 * significantly bigger than if only LoDPI images are loaded. This is a simple workaround only to a point though:
 * https://github.com/mozilla/mozjpeg/issues/76#issuecomment-50140738 It technically needs those optimizations too.
 * This problem is reduced/solved though assumption 3 though.
 *
 * Because I assume that pixel perfect scaling isn't necessary, I opted to only scale to factor 1.75 (~3x the pixels).
 * One of the reasons is because i expect images to be scaled down on mobile (eg. max-width: 100%)
 * wich would normally result in an overshoot resolution.
 * Another reason is that scaling to 2x will significantly increase the image size and compensating for that with
 * reduced jpeg quality results in bad color accuracy. Furthermore, because my previous assumptions limit
 * the max resolution of the retina variant, a small image would get more dpi than a bigger one
 * which makes the page look inconsistent on a desktop screen where assumption 1 fails. Having all images
 * not perfectly scaled reduces that problem, saves bandwidth and reduces risk of overshooting the resolution.
 *
 * There are a few cases where all my assumptions fail:
 * - If you have an image that is always for example 50% wide.
 *   In that case you wouldn't really need the HiDPI variant since it is smaller on mobile.
 *   But since the image is smaller to begin with, this isn't a big issue.
 *   The bigger issue is an image that is always 100% wide but that gets solved by assumption 1.
 * - If the low resolution is an artistic choice eg for blur or pixel art.
 * - When you explicitly optimize for HiDPI desktop and you have large images then this solution will not work.
 *   You'll need to manually optimize images either by using the sizes attribute or the picture element.
 */
class HiDpiHandler
{
    const DEFAULT_OPTIONS = [
        'enabled' => true, // Enables HiDPI handling
        'maxDimension' => 1000, // The highest allowed dimension of the HiDPI image
        'maxFactor' => 3 ** 0.5, // The maximum scaling factor: 3 times the pixels ~ 1.73
        'minFactor' => 2 ** 0.5, // The minimum scaling factor: 2 times the pixels ~ 1.41
        'maxFileSize' => 150 * 1024, // The maximum file size the HiDPI image is allowed to have: 150kb
    ];

    /**
     * @var array
     */
    protected $options;

    public function __construct(array $options = [])
    {
        $extConfOptions = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['hidpi']['options'] ?? [];
        $this->options = $options + $extConfOptions + self::DEFAULT_OPTIONS;
    }

    /**
     * Creates the srcset attribute for either an <img> element or an <source> element.
     *
     * @param FileInterface $image The original file.
     * @param int $width The lowdpi target width of the image.
     * @param int $height The lowdpu target height of the final image.
     * @param array $additional Additional Instructions for the image processor like crop or fileExtension.
     *
     * @return string|null
     * @internal prefer attachSrcSetToTag
     * @see \Hn\HiDPI\HiDpiHandler::attachSrcSetToTag
     */
    public function createSrcSetString(FileInterface $image, int $width, int $height, array $additional = []): ?string
    {
        if (!$this->options['enabled']) {
            return null;
        }

        // don't bother with a HiDPI version for svg's
        if (preg_match('#svg#i', $image->getExtension())) {
            return null;
        }

        $imageWidth = $image->getProperty('width');
        $imageHeight = $image->getProperty('height');
        if (!empty($additional['crop'])) {
            $crop = json_decode($additional['crop'], true);
            $imageWidth = min($imageWidth, $crop['width'] ?? INF);
            $imageHeight = min($imageHeight, $crop['height'] ?? INF);
        }
        self::dispatch('imageOriginalSize', [$image, &$imageWidth, &$imageHeight, $this->options, $additional]);

        $highResScaleFactor = min(
            $this->options['maxFactor'],
            min($this->options['maxDimension'], $imageWidth) / $width,
            min($this->options['maxDimension'], $imageHeight) / $height
        );
        self::dispatch('scaleFactor', [&$highResScaleFactor, $image, $width, $height, $this->options, $additional]);

        if ($highResScaleFactor < $this->options['minFactor']) {
            return null;
        }

        $hWidth = (int)round($width * $highResScaleFactor);
        $hHeight = (int)round($height * $highResScaleFactor);

        $processingInstructions = ['width' => "{$hWidth}c", 'height' => "{$hHeight}c"] + $additional;

        // reduce the quality of jpeg images depending on how high the scaling factor is
        if (preg_match('#jpe?g#i', $this->getTask($image, $processingInstructions)->getTargetFileExtension())) {
            $jpegQuality = MathUtility::forceIntegerInRange($GLOBALS['TYPO3_CONF_VARS']['GFX']['jpg_quality'], 10, 100, 85);
            $hiDpiQuality = round(sqrt($jpegQuality ** 2 / $highResScaleFactor));
            $processingInstructions['additionalParameters'] = "-quality $hiDpiQuality";
            // TODO handle if there already are additional parameters
            // TODO check if it is possible to use q-table's to optimize the low quality image for color accuracy
        }

        self::dispatch('preProcess', [&$processingInstructions, $image, $width, $height, $this->options]);
        $imageService = GeneralUtility::makeInstance(ObjectManager::class)->get(ImageService::class);
        $processedImage = $imageService->applyProcessingInstructions($image, $processingInstructions);
        self::dispatch('postProcess', [&$processedImage, $image, $width, $height, $this->options]);

        if ($processedImage->getProperty('width') !== $hWidth || $processedImage->getProperty('height') !== $hHeight) {
            return null;
        }

        if ($processedImage->getSize() > $this->options['maxFileSize']) {
            return null;
        }

        $roundedScaleFactor = round($highResScaleFactor, strlen(max($width, $height)) + 1);
        return "{$imageService->getImageUri($processedImage)} {$roundedScaleFactor}x";
        // it is fine to omit the 1x variant: https://stackoverflow.com/a/34470921/1973256
    }

    /**
     * @param TagBuilder $tag
     * @param FileInterface $originalFile
     * @param array $additionalInstructions
     */
    public function attachSrcSetToTag(TagBuilder $tag, FileInterface $originalFile, array $additionalInstructions = [])
    {
        if ($tag->getTagName() !== 'img') {
            return;
        }

        $width = intval(html_entity_decode($tag->getAttribute('width')));
        $height = intval(html_entity_decode($tag->getAttribute('height')));
        self::dispatch('parseSize', [$tag, $originalFile, &$width, &$height, $this->options]);
        if ($width <= 0 || $height <= 0) {
            return;
        }

        $srcset = $this->createSrcSetString($originalFile, $width, $height, $additionalInstructions);
        if ($srcset !== null) {
            $tag->addAttribute('srcset', $srcset);
            self::dispatch('postTag', [$tag, $originalFile, $width, $height, $this->options]);
        }
    }

    protected function getTask(FileInterface $file, array $processingInstructions): ?TaskInterface
    {
        if (method_exists($file, 'getOriginalFile')) {
            $file = $file->getOriginalFile();
        }

        if (!$file instanceof File) {
            return null;
        }

        $fakeProcessedFile = GeneralUtility::makeInstance(ProcessedFile::class, $file, ProcessedFile::CONTEXT_IMAGECROPSCALEMASK, $processingInstructions);
        return $fakeProcessedFile->getTask();
    }

    private static function dispatch(string $name, array $arguments)
    {
        GeneralUtility::makeInstance(Dispatcher::class)->dispatch(__CLASS__, $name, $arguments);
    }
}
