<?php

namespace Hn\HiDPI\ViewHelpers;


use Hn\HiDPI\HiDpiHandler;
use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class ImageViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
{
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('hidpi', 'array', "hi dpi options", false, []);
    }

    public function render()
    {
        parent::render();
        $image = $this->imageService->getImage($this->arguments['src'], $this->arguments['image'], $this->arguments['treatIdAsReference']);

        $cropString = $this->arguments['crop'];
        if ($cropString === null && $image->hasProperty('crop') && $image->getProperty('crop')) {
            $cropString = $image->getProperty('crop');
        }
        $cropVariantCollection = CropVariantCollection::create((string)$cropString);
        $cropVariant = $this->arguments['cropVariant'] ?: 'default';
        $cropArea = $cropVariantCollection->getCropArea($cropVariant);

        $hiDpiHandler = GeneralUtility::makeInstance(HiDpiHandler::class, $this->arguments['hidpi'] ?? []);
        $hiDpiHandler->attachSrcSetToTag($this->tag, $image, [
            'crop' => $cropArea->isEmpty() ? null : $cropArea->makeAbsoluteBasedOnFile($image),
        ]);

        return $this->tag->render();
    }

}
