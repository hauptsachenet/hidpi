<?php

namespace Hn\HiDPI\ViewHelpers;


use Hn\HiDPI\HiDpiHandler;
use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * DO NOT use this view helper directly, rather use the normal typo3 media view helper.
 *
 * This override implements a conservative drop-in HiDPI image scaling.
 */
class MediaViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\MediaViewHelper
{
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('hidpi', 'array', "hi dpi options", false, []);
    }

    protected function renderImage(FileInterface $image, $width, $height)
    {
        parent::renderImage($image, $width, $height);

        $cropVariant = $this->arguments['cropVariant'] ?: 'default';
        $cropString = $image instanceof FileReference ? $image->getProperty('crop') : '';
        $cropVariantCollection = CropVariantCollection::create((string)$cropString);
        $cropArea = $cropVariantCollection->getCropArea($cropVariant);

        $hiDpiHandler = GeneralUtility::makeInstance(HiDpiHandler::class, array_replace(
            $this->arguments['additionalConfig']['hidpi'] ?? [],
            $this->arguments['hidpi'] ?? []
        ));
        $hiDpiHandler->attachSrcSetToTag($this->tag, $image, [
            'crop' => $cropArea->isEmpty() ? null : $cropArea->makeAbsoluteBasedOnFile($image),
        ]);

        return $this->tag->render();
    }
}
