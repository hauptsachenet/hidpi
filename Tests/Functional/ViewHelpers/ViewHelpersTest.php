<?php

namespace Hn\HiDPI\Tests\Functional\ViewHelpers;


use Hn\HiDPI\HiDpiHandler;
use Nimut\TestingFramework\TestCase\FunctionalTestCase;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Resource\DuplicationBehavior;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Resource\ProcessedFile;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\CMS\Core\Resource\StorageRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Service\ImageService;
use TYPO3\CMS\Extbase\SignalSlot\Dispatcher;
use TYPO3\CMS\Fluid\View\StandaloneView;

class ViewHelpersTest extends FunctionalTestCase
{
    protected $coreExtensionsToLoad = [
        'recordlist',
    ];

    protected $testExtensionsToLoad = [
        'typo3conf/ext/hidpi',
    ];

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        // for some reason this needs to be defined or else errors will just screw phpunit
        $GLOBALS['TYPO3_CONF_VARS']['LOG'] = [];
    }

    protected function setUp()
    {
        parent::setUp();
        // we create files in this tests so we need authorization
        $GLOBALS['BE_USER'] = $this->createConfiguredMock(BackendUserAuthentication::class, ['isAdmin' => true]);

        $imageService = $this->createMock(ImageService::class);
        GeneralUtility::setSingletonInstance(ImageService::class, $imageService);
        $imageService->method('applyProcessingInstructions')->willReturnCallback(function (FileInterface $file, array $instructions) {
            $processedFile = $this->createMock(ProcessedFile::class);
            $processedFile->method('getOriginalFile')->willReturn($file);
            $processedFile->method('getPublicUrl')->willReturn("/processed/file/{$instructions['width']}/{$instructions['height']}/f.jpg");
            $processedFile->method('getProperty')->willReturnMap([
                ['width', intval($instructions['width'])],
                ['height', intval($instructions['height'])],
            ]);
            $processedFile->method('hasProperty')->willReturnMap([
                ['width', true],
                ['height', true],
            ]);
            return $processedFile;
        });
        $imageService->method('getImageUri')->willReturnCallback(function (ProcessedFile $file) {
            return $file->getPublicUrl();
        });
        $imageService->method('getImage')->willReturnArgument(1);
    }

    protected function tearDown()
    {
        GeneralUtility::purgeInstances();
        unset($GLOBALS['BE_USER']);
        parent::tearDown();
    }

    protected function renderMediaViewHelper(array $arguments)
    {
        $view = new StandaloneView();
        $view->setTemplatePathAndFilename(__DIR__ . '/../../Fixtures/MediaViewHelper.html');
        $view->assignMultiple($arguments);
        return trim($view->render());
    }

    protected function renderImageViewHelper(array $arguments)
    {
        $view = new StandaloneView();
        $view->setTemplatePathAndFilename(__DIR__ . '/../../Fixtures/ImageViewHelper.html');
        $view->assignMultiple($arguments);
        return trim($view->render());
    }

    protected function assertImageTag(string $expectedHtml, array $arguments)
    {
        $this->assertEquals($expectedHtml, $this->renderImageViewHelper($arguments));
        $this->assertEquals($expectedHtml, $this->renderMediaViewHelper($arguments));

    }

    public function testNormal()
    {
        $file = $this->getDemoFile(GeneralUtility::makeInstance(StorageRepository::class)->findByUid(1));
        $this->assertImageTag(
            '<img src="/processed/file/200/150/f.jpg" width="200" height="150" alt="" srcset="/processed/file/346c/260c/f.jpg 1.7321x" />',
            [
                'file' => $file,
                'width' => 200,
                'height' => 150,
            ]
        );
    }

    public function testDisable()
    {
        $file = $this->getDemoFile(GeneralUtility::makeInstance(StorageRepository::class)->findByUid(1));
        $this->assertImageTag(
            '<img src="/processed/file/200/150/f.jpg" width="200" height="150" alt="" />',
            [
                'file' => $file,
                'width' => 200,
                'height' => 150,
                'options' => ['enabled' => 0],
            ]
        );
    }

    public function testSlotModification()
    {
        GeneralUtility::makeInstance(Dispatcher::class)->connect(HiDpiHandler::class, 'parseSize', function ($tag, $originalFile, &$width, &$height) {
            $width = 300;
        });
        $file = $this->getDemoFile(GeneralUtility::makeInstance(StorageRepository::class)->findByUid(1));
        $this->assertImageTag(
            '<img src="/processed/file/200/150/f.jpg" width="200" height="150" alt="" srcset="/processed/file/520c/260c/f.jpg 1.7321x" />',
            [
                'file' => $file,
                'width' => 200,
                'height' => 150,
            ]
        );
    }

    public function testCrop()
    {
        $crop = json_encode([
            'default' => [
                'cropArea' => ['x' => 0, 'y' => 0, 'width' => 300 / 1200, 'height' => 225 / 900],
                'selectedRatio' => "NaN",
                'focusArea' => null,
            ],
        ]);
        $file = $this->getDemoFile(GeneralUtility::makeInstance(StorageRepository::class)->findByUid(1));
        $fileReference = new FileReference(['uid_local' => $file->getUid(),'crop' => $crop]);
        $this->assertNotEmpty($fileReference->getProperty('crop'));
        $this->assertImageTag(
            '<img src="/processed/file/200/150/f.jpg" width="200" height="150" alt="" srcset="/processed/file/300c/225c/f.jpg 1.5x" />',
            [
                'file' => $fileReference,
                'width' => 200,
                'height' => 150,
            ]
        );
    }

    /**
     * @param ResourceStorage $resourceStorage
     *
     * @return FileInterface
     * @throws \TYPO3\CMS\Core\Resource\Exception\ExistingTargetFileNameException
     */
    protected function getDemoFile(ResourceStorage $resourceStorage): FileInterface
    {
        $filePath = __DIR__ . '/../../Fixtures/cat.jpg';
        $file = $resourceStorage->addFile(
            $filePath,
            $resourceStorage->getDefaultFolder(),
            date('YmdHis') . '.jpg',
            DuplicationBehavior::REPLACE,
            false
        );
        return $file;
    }
}
