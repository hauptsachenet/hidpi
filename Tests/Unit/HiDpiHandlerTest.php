<?php

namespace Hn\HiDPI\Tests\Unit;


use Hn\HiDPI\HiDpiHandler;
use Nimut\TestingFramework\TestCase\UnitTestCase;
use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\ProcessedFile;
use TYPO3\CMS\Core\Resource\Processing\TaskInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Service\ImageService;

class HiDpiHandlerTest extends UnitTestCase
{
    /** @var MockObject|ObjectManager */
    protected $objectManager;
    /** @var HiDpiHandler */
    private $hiDpiHandler;
    /** @var ImageService|MockObject */
    private $imageService;

    protected function setUp()
    {
        parent::setUp();
        $this->hiDpiHandler = new HiDpiHandler();

        $this->imageService = $this->createMock(ImageService::class);
        GeneralUtility::setSingletonInstance(ImageService::class, $this->imageService);
        $this->imageService->method('getImageUri')->willReturnCallback(function (FileInterface $file) {
            return $file->getPublicUrl();
        });

        $this->objectManager = $this->createMock(ObjectManager::class);
        GeneralUtility::setSingletonInstance(ObjectManager::class, $this->objectManager);
        $this->objectManager->method('get')->willReturnMap([
            [ImageService::class, $this->imageService],
        ]);

        // this value actually differs between TYPO3 8 and TYPO3 9
        $GLOBALS['TYPO3_CONF_VARS']['GFX']['jpg_quality'] = 85;
    }

    protected function tearDown()
    {
        unset($GLOBALS['TYPO3_CONF_VARS']['GFX']['jpg_quality']);
        GeneralUtility::purgeInstances();
        parent::tearDown();
    }

    public static function cases()
    {
        $sets = [
            'normal' => [900, 600, 900, 1.5, 69],
            'no hi-res available' => [900, 900],
            'dimension limit' => [2000, 625, 1000, 1.6, 67],
            'rounding' => [2000, 500, 866, 1.7321, 65],
            'upsampling' => [200, 500],
            'not good enough' => [600, 500],
            'not good enough 2' => [700, 500],
        ];
        $lowLimitReach = ceil(1000 / 3 ** 0.5);
        $hiLimitReach = floor(1000 / 2 ** 0.5);
        foreach (range($lowLimitReach, $hiLimitReach, ($hiLimitReach - $lowLimitReach) / 10) as $dimension) {
            $dimension = round($dimension);
            $sets["reach limit with $dimension"] = [
                2000,
                $dimension,
                1000,
                round($scaleFactor = 1000 / $dimension, 4),
                round(sqrt(85 ** 2 / $scaleFactor))
            ];
        }
        return $sets;
    }

    public static function hiDpiCases()
    {
        return array_filter(static::cases(), function ($case) {
            return count($case) > 2;
        });
    }

    public static function lowDpiCases()
    {
        return array_filter(static::cases(), function ($case) {
            return count($case) <= 2;
        });
    }

    /**
     * @dataProvider lowDpiCases
     */
    public function testSrcSetLowDpi(int $sourceDimension, int $lowDpiDimension)
    {
        $file = $this->createMock(File::class);
        $file->method('getProperty')->willReturnMap([['width', $sourceDimension], ['height', $sourceDimension]]);
        $file->method('hasProperty')->willReturnMap([['width', true], ['height', true]]);

        $this->imageService->expects($this->never())->method('applyProcessingInstructions');
        $srcset = $this->hiDpiHandler->createSrcSetString($file, $lowDpiDimension, $lowDpiDimension);
        $this->assertNull($srcset);
    }

    /**
     * @dataProvider hiDpiCases
     */
    public function testSrcSetHiDpi(int $sourceDimension, int $lowDpiDimension, int $hiDpiDimension, $scaleFactor)
    {
        $file = $this->createMock(File::class);
        $file->method('getProperty')->willReturnMap([['width', $sourceDimension], ['height', $sourceDimension]]);
        $file->method('hasProperty')->willReturnMap([['width', true], ['height', true]]);

        $processedFile = $this->createMock(ProcessedFile::class);
        $task = $this->createMock(TaskInterface::class);
        $task->method('getTargetFileExtension')->willReturn('png');
        $processedFile->method('getTask')->willReturn($task);
        GeneralUtility::addInstance(ProcessedFile::class, $processedFile);

        $this->imageService->expects($this->once())->method('applyProcessingInstructions')
            ->with($file, [
                'width' => "{$hiDpiDimension}c",
                'height' => "{$hiDpiDimension}c",
            ])
            ->willReturnCallback(function ($file, $instructions) use ($hiDpiDimension) {
                $file = $this->createMock(ProcessedFile::class);
                $file->method('getPublicUrl')->willReturn('hidpi');
                $file->method('getProperty')->willReturnMap([
                    ['width', $hiDpiDimension],
                    ['height', $hiDpiDimension],
                ]);
                return $file;
            });
        $srcset = $this->hiDpiHandler->createSrcSetString($file, $lowDpiDimension, $lowDpiDimension);
        $this->assertEquals("hidpi {$scaleFactor}x", $srcset);
    }

    /**
     * @dataProvider hiDpiCases
     */
    public function testSrcSetJpeg(int $sourceDimension, int $lowDpiDimension, int $hiDpiDimension, $scaleFactor, $quality)
    {
        $file = $this->createMock(File::class);
        $file->method('getProperty')->willReturnMap([['width', $sourceDimension], ['height', $sourceDimension]]);
        $file->method('hasProperty')->willReturnMap([['width', true], ['height', true]]);

        $processedFile = $this->createMock(ProcessedFile::class);
        $task = $this->createMock(TaskInterface::class);
        $task->method('getTargetFileExtension')->willReturn('jpeg');
        $processedFile->method('getTask')->willReturn($task);
        GeneralUtility::addInstance(ProcessedFile::class, $processedFile);

        $this->imageService->expects($this->once())->method('applyProcessingInstructions')
            ->with($file, [
                'width' => "{$hiDpiDimension}c",
                'height' => "{$hiDpiDimension}c",
                'additionalParameters' => "-quality $quality",
            ])
            ->willReturnCallback(function ($file, $instructions) use ($hiDpiDimension) {
                $file = $this->createMock(ProcessedFile::class);
                $file->method('getPublicUrl')->willReturn('hidpi');
                $file->method('getProperty')->willReturnMap([
                    ['width', $hiDpiDimension],
                    ['height', $hiDpiDimension],
                ]);
                return $file;
            });
        $srcset = $this->hiDpiHandler->createSrcSetString($file, $lowDpiDimension, $lowDpiDimension);
        $this->assertEquals("hidpi {$scaleFactor}x", $srcset);
    }
}
