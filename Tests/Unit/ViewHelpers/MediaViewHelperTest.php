<?php

namespace Hn\HiDPI\Tests\Unit\ViewHelpers;


use Hn\HiDPI\ViewHelpers\MediaViewHelper;
use Nimut\TestingFramework\TestCase\ViewHelperBaseTestcase;
use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\ProcessedFile;
use TYPO3\CMS\Core\Resource\Processing\TaskInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Reflection\ReflectionService;
use TYPO3\CMS\Extbase\Service\ImageService;

class MediaViewHelperTest extends ViewHelperBaseTestcase
{
    /**
     * @var MockObject|ImageService
     */
    protected $imageService;

    /**
     * @var MediaViewHelper
     */
    protected $mediaViewHelper;
    /**
     * @var MockObject|ObjectManager
     */
    protected $objectManager;
    /**
     * @var MockObject|File
     */
    protected $file;
    /**
     * @var MockObject|ProcessedFile
     */
    protected $loDpiFile;
    /**
     * @var MockObject|ProcessedFile
     */
    protected $hiDpiFile;

    protected function setUp()
    {
        parent::setUp();
        $this->mediaViewHelper = new MediaViewHelper();
        $this->injectDependenciesIntoViewHelper($this->mediaViewHelper);

        $this->imageService = $this->createMock(ImageService::class);
        GeneralUtility::setSingletonInstance(ImageService::class, $this->imageService);
        $this->objectManager = $this->createMock(ObjectManager::class);
        GeneralUtility::setSingletonInstance(ObjectManager::class, $this->objectManager);
        if (method_exists($this->mediaViewHelper, 'injectObjectManager')) {
            $this->mediaViewHelper->injectObjectManager($this->objectManager);
        }
        $this->objectManager->method('get')->willReturnMap([
            [ImageService::class, $this->imageService]
        ]);

        if (method_exists($this->mediaViewHelper, 'injectReflectionService')) {
            $reflectionService = $this->createConfiguredMock(ReflectionService::class, ['getMethodParameters' => []]);
            $this->mediaViewHelper->injectReflectionService($reflectionService);
        }

        $this->file = $this->createMock(File::class);
        $this->loDpiFile = $this->createMock(ProcessedFile::class);
        $this->hiDpiFile = $this->createMock(ProcessedFile::class);

        // this value actually differs between TYPO3 8 and TYPO3 9
        $GLOBALS['TYPO3_CONF_VARS']['GFX']['jpg_quality'] = 85;
    }

    protected function tearDown()
    {
        unset($GLOBALS['TYPO3_CONF_VARS']['GFX']['jpg_quality']);
        GeneralUtility::purgeInstances();
        parent::tearDown();
    }

    protected function buildArguments(array $arguments)
    {
        $defaultArguments = [];
        $requiredArguments = [];
        foreach ($this->mediaViewHelper->prepareArguments() as $argument) {
            if ($argument->isRequired()) {
                $requiredArguments[$argument->getName()] = $argument->getName();
            } else {
                $defaultArguments[$argument->getName()] = $argument->getDefaultValue();
            }
        }

        $missingRequired = array_diff($requiredArguments, array_keys($arguments));
        $this->assertEquals([], $missingRequired, "All required arguments should be defined");

        return $arguments + $defaultArguments;
    }

    public static function cases()
    {
        $sets = [
            'normal' => [900, 600, 900, 1.5],
            'no hi-res available' => [900, 900],
            'dimension limit' => [2000, 625, 1000, 1.6],
            'rounding' => [2000, 500, 866, 1.7321],
            'upsampling' => [200, 500],
            'not good enough' => [600, 500],
            'not good enough 2' => [700, 500],
        ];
        $lowLimitReach = ceil(1000 / 3 ** 0.5);
        $hiLimitReach = floor(1000 / 2 ** 0.5);
        foreach (range($lowLimitReach, $hiLimitReach, ($hiLimitReach - $lowLimitReach) / 10) as $dimension) {
            $dimension = round($dimension);
            $sets["reach limit with $dimension"] = [2000, $dimension, 1000, round(1000 / $dimension, 4)];
        }
        return $sets;
    }

    /**
     * @dataProvider cases
     */
    public function testNormalOperation(int $sourceDimension, int $lowDpiDimension, int $hiDpiDimension = null, $scaleFactor = null)
    {
        $this->file->method('getProperty')->willReturnMap([
            ['width', $sourceDimension],
            ['height', $sourceDimension],
        ]);
        $this->loDpiFile->method('getProperty')->willReturnMap([
            ['width', $lowDpiDimension],
            ['height', $lowDpiDimension],
        ]);
        $this->hiDpiFile->method('getProperty')->willReturnMap([
            ['width', $hiDpiDimension],
            ['height', $hiDpiDimension],
        ]);

        $this->imageService->expects($this->exactly(1 + ($hiDpiDimension !== null)))
            ->method('applyProcessingInstructions')
            ->withConsecutive(
                [$this->file, ['width' => "{$lowDpiDimension}", 'height' => "{$lowDpiDimension}", 'crop' => null]],
                [$this->file, ['width' => "{$hiDpiDimension}c", 'height' => "{$hiDpiDimension}c", 'crop' => null]]
            )
            ->willReturnOnConsecutiveCalls(
                $this->loDpiFile,
                $this->hiDpiFile
            );
        $this->imageService->expects($this->exactly(1 + ($hiDpiDimension !== null)))
            ->method('getImageUri')
            ->withConsecutive([$this->loDpiFile], [$this->hiDpiFile])
            ->willReturnOnConsecutiveCalls('lodpi', 'hidpi');

        $this->mediaViewHelper->setArguments($this->buildArguments([
            'file' => $this->file,
            'width' => $lowDpiDimension,
            'height' => $lowDpiDimension,
        ]));

        $processedFile = $this->createMock(ProcessedFile::class);
        $task = $this->createMock(TaskInterface::class);
        $task->method('getTargetFileExtension')->willReturn('png');
        $processedFile->method('getTask')->willReturn($task);
        GeneralUtility::addInstance(ProcessedFile::class, $processedFile);

        $this->mediaViewHelper->initialize();
        $result = $this->mediaViewHelper->render();
        if ($hiDpiDimension === null) {
            $this->assertEquals('<img src="lodpi" width="' . $lowDpiDimension . '" height="' . $lowDpiDimension . '" alt="" />', $result);
        } else {
            $this->assertEquals('<img src="lodpi" width="' . $lowDpiDimension . '" height="' . $lowDpiDimension . '" alt="" srcset="hidpi ' . $scaleFactor . 'x" />', $result);
            $this->assertEquals($hiDpiDimension, $lowDpiDimension * $scaleFactor, "The scale factor applied to the low res variant must result in the hi res variant", 0.1);
        }
    }
}
