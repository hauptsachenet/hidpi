<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'HiDPI',
    'version' => '1.0.3',
    'description' => 'Drop-in HiDPI solution. No new view helpers or workflow required. No JavaScript hacks. Just a few assumptions.',
    'category' => 'fe',
    'author' => 'Marco Pfeiffer',
    'author_email' => 'marco@hauptsache.net',
    'author_company' => 'hauptsache.net',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.10-9.5.99',
            'extbase' => '*',
            'fluid' => '*',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
    'state' => 'stable',
    'clearCacheOnLoad' => true,
    'autoload' => [
        'psr-4' => [
            'Hn\\HiDPI\\' => 'Classes',
        ],
    ],
];
