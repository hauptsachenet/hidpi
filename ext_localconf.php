<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

call_user_func(function ($extkey) {
    $conf = class_exists(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)
        ? \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class)->get($extkey)
        : unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$extkey]);

    // this array can be used to change the default options
    // $GLOBALS['TYPO3_CONF_VARS']['EXTCONF'][$extkey]['options'] = [];

    if ($conf['override'] ?? true) {
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Fluid\ViewHelpers\MediaViewHelper::class]['className']
            = \Hn\HiDPI\ViewHelpers\MediaViewHelper::class;
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper::class]['className']
            = \Hn\HiDPI\ViewHelpers\ImageViewHelper::class;
    }
}, $_EXTKEY);
